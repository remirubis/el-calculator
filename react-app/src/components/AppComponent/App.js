import './App.css';
import React from "react";
import Display from "../DisplayComponent/Display";
import Button from "../ButtonComponent/Button";

const btnList = [
  ['AC', '±', '%', '÷'],
  [7, 8, 9, '×'],
  [4, 5, 6, '-'],
  [1, 2, 3, '+'],
  [0, '.', '='],
];

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first: 0,
      second: 0,
      operator: '',
      result: 0
    };
  }

  action(e) {
    e.preventDefault();
    const content = e.target.innerHTML;
    switch (content) {
      case '÷':
        this.setState({ operator: '÷' });
        if (this.state.second !== 0) {
          this.result();
        }
        break;
      case '×':
        this.setState({ operator: '×' });
        if (this.state.second !== 0) {
          this.result();
        }
        break;
      case '-':
        if (this.state.operator !== '×' && this.state.operator !== '÷') {
          this.setState({ operator: '-' });
          if (this.state.second !== 0) {
            this.result();
          }
        } else {
          this.setState({
            second: '-'
          });
        }
        break;
      case '+':
        this.setState({ operator: '+' });
        if (this.state.second !== 0) {
          this.result();
        }
        break;
      case '=':
        this.result();
        break;
      case '±':
        this.inverse();
        break;
      case '%':
        this.pourcent();
        break;
      case 'AC':
        this.reset();
        break;
      case '.':
        if(this.state.second === 0) {
          this.setState((state) => ({
            first: `${state.first}${state.first.toString().includes('.') ? '' : '.'}`
          }));
        } else {
          this.setState((state) => ({
            second: `${state.second}${state.second.toString().includes('.') ? '' : '.'}`
          }));
        }
        break;
    
      default:
        this.number(content);
        break;
    }
  }

  execOperation() {
    let {
      first,
      second,
      operator,
      result
    } = this.state;
    if (Number(second) !== 0 && operator !== '') {
      switch (operator) {
        case '-':
          return Number(first) - Number(second);
        case '+':
          return Number(first) + Number(second);
        case '÷':
          if(Number(second) !== 0) {
            return Number(first) / Number(second);
          } else {
            return 'Erreur';
          }
        case '×':
          if (!Number(second) && result !== 0) return Number(first) * Number(first);
          return Number(first) * Number(second);
      
        default:
          console.error(`[Error] Operator unknow! (output: ${operator})`);
          break;
      }
    } else {
      return Number(first);
    }
  }

  number(content) {
    console.log(this.state);
    if (this.state.first === this.state.result && this.state.operator === '') {
      this.reset();
    }
    if (this.state.first === 0 || this.state.operator === '') {
      this.setState((state) => ({
        first: `${parseFloat(state.first) === 0 && Number(content) > 0
            ? state.first.toString().includes('.') ? state.first : ''
            : state.first
          }${
            Number(content) > 0 && state.first.toString().length <= 8
              ? content
              : state.first.toString().includes('.')
                ? content
                : ''
          }`,
        second: 0,
        operator: '',
        result: state.result
      }));
    } else {
      this.setState((state) => ({
        second: `${state.second === 0  ? '' : state.second}${content === 0 ? '' : content}`
      }));
    }
  }

  result() {
    if (this.state.first.toString() !== '0') {
      const result = this.execOperation();
      this.setState({
        first: result,
        second: 0,
        operator: '',
        result: result,
      });
    }
  }

  inverse() {
    if (this.state.second === 0) {
      this.setState((state) => ({
        first: Number(state.first) ? Number(state.first) * -1 : 0,
      }));
    } else {
      this.setState((state) => ({
        second: Number(state.second) ? Number(state.second) * -1 : 0
      }));
    }
  }

  reset() {
    this.setState({
      first: 0,
      second: 0,
      operator: '',
      result: 0,
    });
  }

  pourcent() {
    let num = 0;
    if (this.state.operator === '') {
      num = this.state.first ? parseFloat(this.state.first) : 0;
      this.setState({
        first: (num /= Math.pow(100, 1))
      });
    } else if (this.state.first === this.state.result) {
      num = this.state.result ? parseFloat(this.state.result) : 0;
      this.setState({
        first: (num /= Math.pow(100, 1)),
        result: num
      });
    } else {
      num = this.state.second ? parseFloat(this.state.second) : 0;
      this.setState({
        second: (num /= Math.pow(100, 1))
      });
    }
  }

  render() {
    let result = this.state.result;
    if (this.state.result === 0) {
      result = this.state.operator !== '' && this.state.second !== 0 ? this.state.second : this.state.first
    }
    return (
      <div className="calculator">
        <Display value={ result } />
        <div className="button-list">
          {
            btnList.flat().map((btn, i) => {
              return (
                <Button
                  key={ i }
                  className={`${ btn === 0 ? 'zero' : '' }${ this.state.operator === btn ? ' active' : '' }`}
                  value={ btn }
                  onClick={ (e) => this.action(e) }
                />
              );
            })
          }
        </div>
      </div>
    );
  }
}

export default App;
